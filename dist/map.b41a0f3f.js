// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  var error;
  for (var i = 0; i < entry.length; i++) {
    try {
      newRequire(entry[i]);
    } catch (e) {
      // Save first error but execute all entries
      if (!error) {
        error = e;
      }
    }
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  parcelRequire = newRequire;

  if (error) {
    // throw error from earlier, _after updating parcelRequire_
    throw error;
  }

  return newRequire;
})({"js/map.js":[function(require,module,exports) {
var points = [[55.818877, 37.346739], [55.823489, 37.497313], [55.769985, 37.620704], [55.761182, 37.632577], [55.744823, 37.566990], [55.916678, 37.759264], [55.797677, 37.938952], [55.752134, 37.592166], [55.780392, 37.593201], [43.321581, 45.685319], [51.667630, 39.206473], [56.317482, 43.995502], [54.724241, 55.945486], [59.928593, 30.360734], [47.229064, 39.744267], [55.029799, 82.913770], [56.835924, 60.594803], [45.033734, 38.974175], [45.044202, 41.970943], [54.715004, 20.504873], [53.189717, 45.015980], [43.589791, 39.725965], [62.033200, 129.746515], [57.154278, 65.533685], [55.156029, 61.371032], [43.034840, 44.680798], [61.239458, 73.373251], [54.306575, 48.359644], [60.940092, 76.546209], [44.554297, 38.065193], [50.260653, 127.536328]];
var dataBaloon = [{
  "Наименование ресторана": "Black Star Burger",
  "Город": "Красногорск",
  "Адрес": "ул. Знаменская д. 5"
}, {
  "Наименование ресторана": "Black Star Burger",
  "Город": "Москва",
  "Адрес": "Ленинградское шоссе, д. 16А, ст. 4"
}, {
  "Наименование ресторана": "Black Star Burger",
  "Город": "Москва",
  "Адрес": "ул. Цветной бульвар, д. 11/2"
}, {
  "Наименование ресторана": "Black Star Burger",
  "Город": "Москва",
  "Адрес": "ул. Мсяницкая, д. 18"
}, {
  "Наименование ресторана": "Black Star Burger",
  "Город": "Москва",
  "Адрес": "площадь Киевского вокзала, д. 2"
}, {
  "Наименование ресторана": "Black Star Burger",
  "Город": "Мытищи",
  "Адрес": "Шараповский проезд, д. 2"
}, {
  "Наименование ресторана": "Black Star Burger",
  "Город": "Балашиха",
  "Адрес": "проспект Ленина, д. 25"
}, {
  "Наименование ресторана": "Black Star Burger",
  "Город": "Москва",
  "Адрес": "ул. Новый Арбат, д. 15"
}, {
  "Наименование ресторана": "Black Star Burger",
  "Город": "Москва",
  "Адрес": "ул. Лесная,д. 20, ст. 3"
}, {
  "Наименование ресторана": "Black Star Burger",
  "Город": "Грозный",
  "Адрес": "Проспект В.Путина, д. 5"
}, {
  "Наименование ресторана": "Black Star Burger",
  "Город": "Воронеж",
  "Адрес": "Проспект Револющии, д 38"
}, {
  "Наименование ресторана": "Black Star Burger",
  "Город": "Нижний Новгород",
  "Адрес": "ул. Большая Покровская, д. 42"
}, {
  "Наименование ресторана": "Black Star Burger",
  "Город": "Уфа",
  "Адрес": "ул. Верхнеторговая площадь,д.  6"
}, {
  "Наименование ресторана": "Black Star Burger",
  "Город": "Санкт-Петербург",
  "Адрес": "Лиговский проспект,д. 30"
}, {
  "Наименование ресторана": "Black Star Burger",
  "Город": "Ростов-на-Дону",
  "Адрес": "Театральная площадь,д.  3"
}, {
  "Наименование ресторана": "Black Star Burger",
  "Город": "Новосибирск",
  "Адрес": "ул. Ленина, д. 12"
}, {
  "Наименование ресторана": "Black Star Burger",
  "Город": "Екатеринбург",
  "Адрес": "ул. Вайнера, д. 12"
}, {
  "Наименование ресторана": "Black Star Burger",
  "Город": "Краснодар",
  "Адрес": "ул. Красная, д. 118"
}, {
  "Наименование ресторана": "Black Star Burger",
  "Город": "Ставрополь",
  "Адрес": "ул. Дзержинского, д.  114"
}, {
  "Наименование ресторана": "Black Star Burger",
  "Город": "Калининград",
  "Адрес": "Проспект Ленинский, д. 35\37"
}, {
  "Наименование ресторана": "Black Star Burger",
  "Город": "Пенза",
  "Адрес": "ул. Московская, д 37"
}, {
  "Наименование ресторана": "Black Star Burger",
  "Город": "Сочи",
  "Адрес": "ул. Горького, д.53"
}, {
  "Наименование ресторана": "Black Star Burger",
  "Город": "Якутск",
  "Адрес": "ул. Ленина, д. 6\1"
}, {
  "Наименование ресторана": "Black Star Burger",
  "Город": "Тюмень",
  "Адрес": "ул. Ленина, д. 46"
}, {
  "Наименование ресторана": "Black Star Burger",
  "Город": "Челябинск",
  "Адрес": "ул. Тернопольская, д.5"
}, {
  "Наименование ресторана": "Black Star Burger",
  "Город": "Владикавказ",
  "Адрес": "ул.Кирова, д. 40 А"
}, {
  "Наименование ресторана": "Black Star Burger",
  "Город": "Сургут",
  "Адрес": "Югорский тракт, д. 38"
}, {
  "Наименование ресторана": "Black Star Burger",
  "Город": "Ульяновск",
  "Адрес": "Москвовское шоссе, д 108"
}, {
  "Наименование ресторана": "Black Star Burger",
  "Город": "Нижневартовск",
  "Адрес": "ул. Ленина, д. 15 П"
}, {
  "Наименование ресторана": "Black Star Burger",
  "Город": "Геленджик",
  "Адрес": "Крымская ул., 3/3"
}, {
  "Наименование ресторана": "Black Star Burger",
  "Город": "Благовещенск",
  "Адрес": "Ул. Зейская, 175/179"
}];

function initMap() {
  var map = new ymaps.Map('map', {
    center: [55.746215, 37.622504],
    zoom: 12,
    behaviors: ['default', 'scrollZoom']
  });
  var MyBalloonLayout = ymaps.templateLayoutFactory.createClass('<div class="popover top" style="box-shadow: 0px 6px 11px 0px rgba(0,0,0,0.75);">' + '<div class="arrow"></div>' + '<div class="popover-inner">' + '$[[options.contentLayout observeSize minWidth=180 maxWidth=200 maxHeight=350]]' + '</div>' + '</div>', {
    build: function build() {
      this.constructor.superclass.build.call(this);
      this._$element = $('.popover', this.getParentElement());
      this.applyElementOffset();

      this._$element.find('.close').on('click', $.proxy(this.onCloseClick, this));
    },
    clear: function clear() {
      this._$element.find('.close').off('click');

      this.constructor.superclass.clear.call(this);
    },
    onSublayoutSizeChange: function onSublayoutSizeChange() {
      MyBalloonLayout.superclass.onSublayoutSizeChange.apply(this, arguments);

      if (!this._isElement(this._$element)) {
        return;
      }

      this.applyElementOffset();
      this.events.fire('shapechange');
    },
    applyElementOffset: function applyElementOffset() {
      this._$element.css({
        left: -(this._$element[0].offsetWidth / 2),
        top: -(this._$element[0].offsetHeight + this._$element.find('.arrow')[0].offsetHeight)
      });
    },
    onCloseClick: function onCloseClick(e) {
      e.preventDefault();
      this.events.fire('userclose');
    },
    getShape: function getShape() {
      if (!this._isElement(this._$element)) {
        return MyBalloonLayout.superclass.getShape.call(this);
      }

      var position = this._$element.position();

      return new ymaps.shape.Rectangle(new ymaps.geometry.pixel.Rectangle([[position.left, position.top], [position.left + this._$element[0].offsetWidth, position.top + this._$element[0].offsetHeight + this._$element.find('.arrow')[0].offsetHeight]]));
    },
    _isElement: function _isElement(element) {
      return element && element[0] && element.find('.arrow')[0];
    }
  });
  var MyBalloonContentLayout = ymaps.templateLayoutFactory.createClass('<div class="popover-titlez" style="font-size: 20px; text-transform: uppercase;">$[properties.balloonHeader]</div>' + '<div class="popover-contentz">$[properties.balloonContent]</div>' + '<div class="popover-contentz">$[properties.balloonAddress]</div>');
  var clusterer = new ymaps.Clusterer({
    clusterIconLayout: ymaps.templateLayoutFactory.createClass('<a class="cluster-icon">{{ properties.geoObjects.length }}</a>'),
    clusterIconShape: {
      type: 'Rectangle',
      coordinates: [[0, 0], [40, 40]]
    }
  });
  getPointData = function getPointData(index) {
    return {
      balloonHeader: dataBaloon[index]['Наименование ресторана'],
      balloonContent: dataBaloon[index]['Город'],
      balloonAddress: dataBaloon[index]['Адрес']
    };
  }, iconLayout = ymaps.templateLayoutFactory.createClass('<div class="map-marker"></div>'), getPointOptions = function getPointOptions() {
    return {
      preset: 'custom#examplePreset1',
      iconLayout: iconLayout,
      iconContentOffset: [-150, -150],
      hideIconOnBalloonOpen: false,
      balloonOffset: [0, -3],
      balloonShadow: true,
      balloonLayout: MyBalloonLayout,
      balloonContentLayout: MyBalloonContentLayout,
      balloonPanelMaxMapArea: 0,
      iconShape: {
        type: 'Rectangle',
        coordinates: [[-20, -20], [20, 20]]
      }
    };
  }, geoObjects = [];

  for (var i = 0, len = points.length; i < len; i++) {
    geoObjects[i] = new ymaps.Placemark(points[i], getPointData(i), getPointOptions());
  }

  clusterer.add(geoObjects);
  map.geoObjects.add(clusterer);
  window.map = map;
}

;
$(document).ready(function () {
  ymaps.ready(function () {
    initMap();
  });
});
},{}],"../../../../usr/lib/node_modules/parcel-bundler/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var checkedAssets, assetsToAccept;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "45163" + '/');

  ws.onmessage = function (event) {
    checkedAssets = {};
    assetsToAccept = [];
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      var handled = false;
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          var didAccept = hmrAcceptCheck(global.parcelRequire, asset.id);

          if (didAccept) {
            handled = true;
          }
        }
      }); // Enable HMR for CSS by default.

      handled = handled || data.assets.every(function (asset) {
        return asset.type === 'css' && asset.generated.js;
      });

      if (handled) {
        console.clear();
        data.assets.forEach(function (asset) {
          hmrApply(global.parcelRequire, asset);
        });
        assetsToAccept.forEach(function (v) {
          hmrAcceptRun(v[0], v[1]);
        });
      } else {
        window.location.reload();
      }
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAcceptCheck(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAcceptCheck(bundle.parent, id);
  }

  if (checkedAssets[id]) {
    return;
  }

  checkedAssets[id] = true;
  var cached = bundle.cache[id];
  assetsToAccept.push([bundle, id]);

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAcceptCheck(global.parcelRequire, id);
  });
}

function hmrAcceptRun(bundle, id) {
  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }
}
},{}]},{},["../../../../usr/lib/node_modules/parcel-bundler/src/builtins/hmr-runtime.js","js/map.js"], null)
//# sourceMappingURL=/map.b41a0f3f.js.map