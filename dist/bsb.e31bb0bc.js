// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  var error;
  for (var i = 0; i < entry.length; i++) {
    try {
      newRequire(entry[i]);
    } catch (e) {
      // Save first error but execute all entries
      if (!error) {
        error = e;
      }
    }
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  parcelRequire = newRequire;

  if (error) {
    // throw error from earlier, _after updating parcelRequire_
    throw error;
  }

  return newRequire;
})({"index.js":[function(require,module,exports) {
var locations = {
  moscow: [55.746215, 37.622504],
  spb: [59.939095, 30.315868],
  ufa: [54.735147, 55.958727],
  nn: [56.326797, 44.006516],
  voronezh: [51.660781, 39.200269],
  grozniy: [43.317881, 45.695358],
  ekb: [56.838011, 60.597465],
  novosib: [55.030199, 82.920430],
  krasnodar: [45.035470, 38.975313],
  krasnogorsk: [55.831099, 37.330192],
  rnd: [47.222078, 39.720349],
  mytishchi: [55.910483, 37.736402],
  balashiha: [55.796339, 37.938199],
  stavropol: [45.044521, 41.969083],
  kaliningrad: [54.710454, 20.512733],
  penza: [53.195063, 45.018316],
  sochi: [43.585525, 39.723062],
  yakutsk: [62.027757, 129.731235],
  tyumen: [57.153033, 65.534328],
  chelyabinsk: [55.159897, 61.402554],
  vladikavkaz: [43.021150, 44.681960],
  surgut: [61.254035, 73.396221],
  ulyanovsk: [54.314192, 48.403123],
  nizhnevartovsk: [60.938545, 76.558902],
  gelendzhik: [44.561141, 38.076809],
  blagoveschensk: [50.290640, 127.527173]
};
window.locations = locations;
$(document).ready(function () {
  if (!window.city) {
    window.city = "moscow";
  }

  $('input[type=file]').change(function (e) {
    $in = $(this);
    $in.next().html($in.val());
  });
  $('.uploadButton').click(function () {
    var fileName = $("#fileUpload").val();

    if (fileName) {
      alert(fileName + " can be uploaded.");
    } else {
      alert("Please select a file to upload");
    }
  });
  $('#qr-code').qrcode({
    text: "http://www.google.com",
    width: 80,
    height: 80,
    background: "#ffffff",
    foreground: "#000000"
  });
  $(".list-of-cities li, .dropdown-menu li").click(function (e) {
    e.preventDefault();
    $(".list-of-cities li, .dropdown-menu li").removeClass('city-active');
    $(this).addClass('city-active');
    $(".dropdown .btn").html($(this).text());
    $(".dropdown .btn").append('<span><img src="/arrow-down.89ffe8bc.svg" height="9px" width="16px /"></span>');
    var selectedLocation = $(this).data('location');
    map.setCenter(locations[selectedLocation]);
    window.city = selectedLocation;
  });
  $(".btn-reg").click(function () {
    $("#myModal").modal('show');
    $("#registration").removeClass("no-active");
    $("#forgot-pswd").addClass("no-active");
    $("#restore-pswd").addClass("no-active");
    $("#login").addClass("no-active");
    $("#edit-profile").addClass("no-active");
    $(".registration a[href^='#registration']").addClass("isActive");
  });
  $(".edit-btn").click(function () {
    $("#myModal").modal('show');
    $("#edit-profile").removeClass("no-active");
    $("#registration").addClass("no-active");
    $("#forgot-pswd").addClass("no-active");
    $("#restore-pswd").addClass("no-active");
    $("#login").addClass("no-active");
  });
  $(".registration a[href^='#login']").click(function () {
    $("#registration").addClass("no-active");
    $(".registration a[href^='#registration']").removeClass("isActive");
    $("#login").removeClass("no-active");
    $(".login a[href^='#login']").addClass("isActive");
  });
  $(".login a[href^='#registration']").click(function () {
    $("#login").addClass("no-active");
    $(".login a[href^='#login']").removeClass("isActive");
    $("#registration").removeClass("no-active");
    $(".registration a[href^='#registration']").addClass("isActive");
  });
  $("a[href^='#forgot-pswd']").click(function () {
    $("#forgot-pswd").removeClass("no-active");
    $("#registration").addClass("no-active");
    $("#login").addClass("no-active");
    $(".registration a[href^='#registration']").removeClass("isActive");
    $(".login a[href^='#login']").removeClass("isActive");
  });
  $("#forgot-pswd .modal-footer .btn-primary").click(function () {
    $("#forgot-pswd").addClass("no-active");
    $("#restore-pswd").removeClass("no-active");
  });
  $(".back").click(function () {
    $("#forgot-pswd").addClass("no-active");
    $("#login").removeClass("no-active");
    $(".login a[href^='#login']").addClass("isActive");
  });
  $("#restore-pswd .modal-footer .btn-primary").click(function () {
    $("#restore-pswd").addClass("no-active");
    $("#myModal").modal('hide');
  });
  $("").click(function () {
    $("#restore-pswd").addClass("no-active");
    $("#myModal").modal('hide');
  });
  $('.menu__burger').on('click', function (e) {
    e.preventDefault;
    $(this).toggleClass('menu__burger--active');
    $('.menu-popup').toggleClass('active');
  });
  $(window).scroll(hideBlock);

  function hideBlock() {
    $('.menu-popup').removeClass('active');
    $('.menu__burger').removeClass('menu__burger--active');
  }

  $('.lishka').on('click', function (e) {
    e.preventDefault();
  });
});
},{}],"../../../../usr/lib/node_modules/parcel-bundler/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var checkedAssets, assetsToAccept;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "45163" + '/');

  ws.onmessage = function (event) {
    checkedAssets = {};
    assetsToAccept = [];
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      var handled = false;
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          var didAccept = hmrAcceptCheck(global.parcelRequire, asset.id);

          if (didAccept) {
            handled = true;
          }
        }
      }); // Enable HMR for CSS by default.

      handled = handled || data.assets.every(function (asset) {
        return asset.type === 'css' && asset.generated.js;
      });

      if (handled) {
        console.clear();
        data.assets.forEach(function (asset) {
          hmrApply(global.parcelRequire, asset);
        });
        assetsToAccept.forEach(function (v) {
          hmrAcceptRun(v[0], v[1]);
        });
      } else {
        window.location.reload();
      }
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAcceptCheck(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAcceptCheck(bundle.parent, id);
  }

  if (checkedAssets[id]) {
    return;
  }

  checkedAssets[id] = true;
  var cached = bundle.cache[id];
  assetsToAccept.push([bundle, id]);

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAcceptCheck(global.parcelRequire, id);
  });
}

function hmrAcceptRun(bundle, id) {
  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }
}
},{}]},{},["../../../../usr/lib/node_modules/parcel-bundler/src/builtins/hmr-runtime.js","index.js"], null)
//# sourceMappingURL=/bsb.e31bb0bc.js.map