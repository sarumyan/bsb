var points = [
  [55.818877, 37.346739], [55.823489, 37.497313], [55.769985, 37.620704], [55.761182, 37.632577], [55.744823, 37.566990], [55.916678, 37.759264], [55.797677, 37.938952], [55.752134, 37.592166], [55.780392, 37.593201], [43.321581, 45.685319], [51.667630, 39.206473], [56.317482, 43.995502], [54.724241, 55.945486], [59.928593, 30.360734], [47.229064, 39.744267], [55.029799, 82.913770], [56.835924, 60.594803], [45.033734, 38.974175], [45.044202, 41.970943], [54.715004, 20.504873], [53.189717, 45.015980], [43.589791, 39.725965], [62.033200, 129.746515], [57.154278, 65.533685], [55.156029, 61.371032], [43.034840, 44.680798], [61.239458, 73.373251], [54.306575, 48.359644], [60.940092, 76.546209], [44.554297, 38.065193], [50.260653, 127.536328]
];

var dataBaloon = [
  {
    "Наименование ресторана": "Black Star Burger",
    "Город": "Красногорск",
    "Адрес": "ул. Знаменская д. 5"
  },
  {
    "Наименование ресторана": "Black Star Burger",
    "Город": "Москва",
    "Адрес": "Ленинградское шоссе, д. 16А, ст. 4"
  },
  {
    "Наименование ресторана": "Black Star Burger",
    "Город": "Москва",
    "Адрес": "ул. Цветной бульвар, д. 11/2"
  },
  {
    "Наименование ресторана": "Black Star Burger",
    "Город": "Москва",
    "Адрес": "ул. Мсяницкая, д. 18"
  },
  {
    "Наименование ресторана": "Black Star Burger",
    "Город": "Москва",
    "Адрес": "площадь Киевского вокзала, д. 2"
  },
  {
    "Наименование ресторана": "Black Star Burger",
    "Город": "Мытищи",
    "Адрес": "Шараповский проезд, д. 2"
  },
  {
    "Наименование ресторана": "Black Star Burger",
    "Город": "Балашиха",
    "Адрес": "проспект Ленина, д. 25"
  },
  {
    "Наименование ресторана": "Black Star Burger",
    "Город": "Москва",
    "Адрес": "ул. Новый Арбат, д. 15"
  },
  {
    "Наименование ресторана": "Black Star Burger",
    "Город": "Москва",
    "Адрес": "ул. Лесная,д. 20, ст. 3"
  },
  {
    "Наименование ресторана": "Black Star Burger",
    "Город": "Грозный",
    "Адрес": "Проспект В.Путина, д. 5"
  },
  {
    "Наименование ресторана": "Black Star Burger",
    "Город": "Воронеж",
    "Адрес": "Проспект Револющии, д 38"
  },
  {
    "Наименование ресторана": "Black Star Burger",
    "Город": "Нижний Новгород",
    "Адрес": "ул. Большая Покровская, д. 42"
  },
  {
    "Наименование ресторана": "Black Star Burger",
    "Город": "Уфа",
    "Адрес": "ул. Верхнеторговая площадь,д.  6"
  },
  {
    "Наименование ресторана": "Black Star Burger",
    "Город": "Санкт-Петербург",
    "Адрес": "Лиговский проспект,д. 30"
  },
  {
    "Наименование ресторана": "Black Star Burger",
    "Город": "Ростов-на-Дону",
    "Адрес": "Театральная площадь,д.  3"
  },
  {
    "Наименование ресторана": "Black Star Burger",
    "Город": "Новосибирск",
    "Адрес": "ул. Ленина, д. 12"
  },
  {
    "Наименование ресторана": "Black Star Burger",
    "Город": "Екатеринбург",
    "Адрес": "ул. Вайнера, д. 12"
  },
  {
    "Наименование ресторана": "Black Star Burger",
    "Город": "Краснодар",
    "Адрес": "ул. Красная, д. 118"
  },
  {
    "Наименование ресторана": "Black Star Burger",
    "Город": "Ставрополь",
    "Адрес": "ул. Дзержинского, д.  114"
  },
  {
    "Наименование ресторана": "Black Star Burger",
    "Город": "Калининград",
    "Адрес": "Проспект Ленинский, д. 35\37"
  },
  {
    "Наименование ресторана": "Black Star Burger",
    "Город": "Пенза",
    "Адрес": "ул. Московская, д 37"
  },
  {
    "Наименование ресторана": "Black Star Burger",
    "Город": "Сочи",
    "Адрес": "ул. Горького, д.53"
  },
  {
    "Наименование ресторана": "Black Star Burger",
    "Город": "Якутск",
    "Адрес": "ул. Ленина, д. 6\1"
  },
  {
    "Наименование ресторана": "Black Star Burger",
    "Город": "Тюмень",
    "Адрес": "ул. Ленина, д. 46"
  },
  {
    "Наименование ресторана": "Black Star Burger",
    "Город": "Челябинск",
    "Адрес": "ул. Тернопольская, д.5"
  },
  {
    "Наименование ресторана": "Black Star Burger",
    "Город": "Владикавказ",
    "Адрес": "ул.Кирова, д. 40 А"
  },
  {
    "Наименование ресторана": "Black Star Burger",
    "Город": "Сургут",
    "Адрес": "Югорский тракт, д. 38"
  },
  {
    "Наименование ресторана": "Black Star Burger",
    "Город": "Ульяновск",
    "Адрес": "Москвовское шоссе, д 108"
  },
  {
    "Наименование ресторана": "Black Star Burger",
    "Город": "Нижневартовск",
    "Адрес": "ул. Ленина, д. 15 П"
  },
  {
    "Наименование ресторана": "Black Star Burger",
    "Город": "Геленджик",
    "Адрес": "Крымская ул., 3/3"
  },
  {
    "Наименование ресторана": "Black Star Burger",
    "Город": "Благовещенск",
    "Адрес": "Ул. Зейская, 175/179"
  },
];


function initMap() {
  var map = new ymaps.Map('map', {
    center: [55.746215, 37.622504],
    zoom: 12,
    behaviors: ['default', 'scrollZoom']
  });

  var MyBalloonLayout = ymaps.templateLayoutFactory.createClass(
    '<div class="popover top" style="box-shadow: 0px 6px 11px 0px rgba(0,0,0,0.75);">' +
    '<div class="arrow"></div>' +
    '<div class="popover-inner">' +
    '$[[options.contentLayout observeSize minWidth=180 maxWidth=200 maxHeight=350]]' +
    '</div>' +
    '</div>', {
      build: function () {
        this.constructor.superclass.build.call(this);
        this._$element = $('.popover', this.getParentElement());
        this.applyElementOffset();
        this._$element.find('.close')
          .on('click', $.proxy(this.onCloseClick, this));
      },


      clear: function () {
        this._$element.find('.close')
          .off('click');

        this.constructor.superclass.clear.call(this);
      },

      onSublayoutSizeChange: function () {
        MyBalloonLayout.superclass.onSublayoutSizeChange.apply(this, arguments);

        if (!this._isElement(this._$element)) {
          return;
        }

        this.applyElementOffset();

        this.events.fire('shapechange');
      },

      applyElementOffset: function () {
        this._$element.css({
          left: -(this._$element[0].offsetWidth / 2),
          top: -(this._$element[0].offsetHeight + this._$element.find('.arrow')[0].offsetHeight)
        });
      },

      onCloseClick: function (e) {
        e.preventDefault();

        this.events.fire('userclose');
      },

      getShape: function () {
        if (!this._isElement(this._$element)) {
          return MyBalloonLayout.superclass.getShape.call(this);
        }

        var position = this._$element.position();

        return new ymaps.shape.Rectangle(new ymaps.geometry.pixel.Rectangle([
          [position.left, position.top], [
            position.left + this._$element[0].offsetWidth,
            position.top + this._$element[0].offsetHeight + this._$element.find('.arrow')[0].offsetHeight
          ]
        ]));
      },

      _isElement: function (element) {
        return element && element[0] && element.find('.arrow')[0];
      }
    });

  var MyBalloonContentLayout = ymaps.templateLayoutFactory.createClass(
    '<div class="popover-titlez" style="font-size: 20px; text-transform: uppercase;">$[properties.balloonHeader]</div>' +
    '<div class="popover-contentz">$[properties.balloonContent]</div>' +
    '<div class="popover-contentz">$[properties.balloonAddress]</div>'
  );

  var clusterer = new ymaps.Clusterer({
    clusterIconLayout: ymaps.templateLayoutFactory.createClass(
      '<a class="cluster-icon">{{ properties.geoObjects.length }}</a>'
    ),
    clusterIconShape: {
      type: 'Rectangle',
      coordinates: [[0, 0], [40, 40]]
    }
  });

  getPointData = function (index) {
    return {
      balloonHeader: dataBaloon[index]['Наименование ресторана'],
      balloonContent: dataBaloon[index]['Город'],
      balloonAddress: dataBaloon[index]['Адрес'],
    };
  },

    iconLayout = ymaps.templateLayoutFactory.createClass('<div class="map-marker"></div>'),


    getPointOptions = function () {
      return {
        preset: 'custom#examplePreset1',
        iconLayout: iconLayout,
        iconContentOffset: [-150, -150],
        hideIconOnBalloonOpen: false,
        balloonOffset: [0, -3],

        balloonShadow: true,
        balloonLayout: MyBalloonLayout,
        balloonContentLayout: MyBalloonContentLayout,
        balloonPanelMaxMapArea: 0,


        iconShape: {
          type: 'Rectangle',
          coordinates: [
            [-20, -20],
            [20, 20]
          ]

        },

      };
    },
    
    geoObjects = [];

  for (var i = 0, len = points.length; i < len; i++) {
    geoObjects[i] = new ymaps.Placemark(
      points[i],
      getPointData(i),
      getPointOptions()
    );
  }

  clusterer.add(geoObjects);
  map.geoObjects.add(clusterer);

  window.map = map;
};

$(document).ready(function () {
  ymaps.ready(function () {
    initMap();
  });
})
