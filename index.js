var locations = {
  moscow: [55.746215, 37.622504],
  spb: [59.939095, 30.315868],
  ufa: [54.735147, 55.958727],
  nn: [56.326797, 44.006516],
  voronezh: [51.660781, 39.200269],
  grozniy: [43.317881, 45.695358],
  ekb: [56.838011, 60.597465],
  novosib: [55.030199, 82.920430],
  krasnodar: [45.035470, 38.975313],
  krasnogorsk: [55.831099, 37.330192],
  rnd: [47.222078, 39.720349],
  mytishchi: [55.910483, 37.736402],
  balashiha: [55.796339, 37.938199],
  stavropol: [45.044521, 41.969083],
  kaliningrad: [54.710454, 20.512733],
  penza: [53.195063, 45.018316],
  sochi: [43.585525, 39.723062],
  yakutsk: [62.027757, 129.731235],
  tyumen: [57.153033, 65.534328],
  chelyabinsk: [55.159897, 61.402554],
  vladikavkaz: [43.021150, 44.681960],
  surgut: [61.254035, 73.396221],
  ulyanovsk: [54.314192, 48.403123],
  nizhnevartovsk: [60.938545, 76.558902],
  gelendzhik: [44.561141, 38.076809],
  blagoveschensk: [50.290640, 127.527173]
};

window.locations = locations;

$(document).ready(function () {
  if (!window.city) {
    window.city = "moscow"
  }

  $('input[type=file]').change(function (e) {
    $in = $(this);
    $in.next().html($in.val());

  });

  $('.uploadButton').click(function () {
    var fileName = $("#fileUpload").val();
    if (fileName) {
      alert(fileName + " can be uploaded.");
    }
    else {
      alert("Please select a file to upload");
    }
  });


  $('#qr-code').qrcode({
    text: "http://www.google.com",
    width: 80,
    height: 80,
    background: "#ffffff",
    foreground: "#000000"
  });

  $(".list-of-cities li, .dropdown-menu li").click(function (e) {
    e.preventDefault();
    $(".list-of-cities li, .dropdown-menu li").removeClass('city-active');
    $(this).addClass('city-active');
    
    $(".dropdown .btn").html($(this).text());

    $(".dropdown .btn").append('<span><img src="/arrow-down.89ffe8bc.svg" height="9px" width="16px /"></span>')

    var selectedLocation = $(this).data('location');

    map.setCenter(locations[selectedLocation]);

    window.city = selectedLocation
  })

  $(".btn-reg").click(function () {
    $("#myModal").modal('show');
    $("#registration").removeClass("no-active");
    $("#forgot-pswd").addClass("no-active");
    $("#restore-pswd").addClass("no-active");
    $("#login").addClass("no-active");
    $("#edit-profile").addClass("no-active");
    $(".registration a[href^='#registration']").addClass("isActive");
  });

  $(".edit-btn").click(function () {
    $("#myModal").modal('show');
    $("#edit-profile").removeClass("no-active");
    $("#registration").addClass("no-active");
    $("#forgot-pswd").addClass("no-active");
    $("#restore-pswd").addClass("no-active");
    $("#login").addClass("no-active");
  });


  $(".registration a[href^='#login']").click(function () {
    $("#registration").addClass("no-active");
    $(".registration a[href^='#registration']").removeClass("isActive");
    $("#login").removeClass("no-active");
    $(".login a[href^='#login']").addClass("isActive");
  });

  $(".login a[href^='#registration']").click(function () {
    $("#login").addClass("no-active");
    $(".login a[href^='#login']").removeClass("isActive");
    $("#registration").removeClass("no-active");
    $(".registration a[href^='#registration']").addClass("isActive");
  });

  $("a[href^='#forgot-pswd']").click(function () {
    $("#forgot-pswd").removeClass("no-active");
    $("#registration").addClass("no-active");
    $("#login").addClass("no-active");
    $(".registration a[href^='#registration']").removeClass("isActive");
    $(".login a[href^='#login']").removeClass("isActive");
  });

  $("#forgot-pswd .modal-footer .btn-primary").click(function () {
    $("#forgot-pswd").addClass("no-active");
    $("#restore-pswd").removeClass("no-active");
  });

  $(".back").click(function () {
    $("#forgot-pswd").addClass("no-active");
    $("#login").removeClass("no-active");
    $(".login a[href^='#login']").addClass("isActive");
  });

  $("#restore-pswd .modal-footer .btn-primary").click(function () {
    $("#restore-pswd").addClass("no-active");
    $("#myModal").modal('hide');
  });

  $("").click(function () {
    $("#restore-pswd").addClass("no-active");
    $("#myModal").modal('hide');
  });

  $('.menu__burger').on('click', function (e) {
    e.preventDefault;
    $(this).toggleClass('menu__burger--active');
    $('.menu-popup').toggleClass('active');
  })

  $(window).scroll(hideBlock);
  function hideBlock() {
    $('.menu-popup').removeClass('active');
    $('.menu__burger').removeClass('menu__burger--active')
  }

  $('.lishka').on('click', function (e) {
    e.preventDefault();
  })
})